package ejemploInicial;

import java.io.Serializable;

/**
 * Created by Profesor on 23/09/2019.
 */
public class Juego implements Serializable{
    private String titulo;
    private float precio;

    public Juego(String titulo, float precio) {
        this.titulo = titulo;
        this.precio = precio;
    }

    public Juego() {
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }


    @Override
    public String toString() {
        return titulo + " pvp: " + precio;
    }

}
