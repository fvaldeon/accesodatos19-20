package ejemploInicial;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.*;
import java.util.HashSet;

/**
 * Created by Profesor on 23/09/2019.
 */
public class Ventana {

    private JTextField txtTitulo;
    private JTextField txtPrecio;
    private JTextArea txtArea;
    private JButton nuevoBtn;
    private JPanel panel1;
    private JLabel lblCantidadJuegos;
    private JLabel lblLongitudTitulo;
    private JButton cargarDatosButton;
    private JButton guardarDatosButton;
    private HashSet<Juego> coleccionJuegos;


    public Ventana() {
        JFrame frame = new JFrame("Ventana");

        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

        frame.setLocationRelativeTo(null);

        coleccionJuegos = new HashSet<>();

        nuevoBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                //Crear un nuevo objeto de tipo Juego

                //Los valores de titulo y precio los voy a sacar
                //De los campos de texto

                Juego juego = new Juego();
                String titulo = txtTitulo.getText();
                float precio = Float.parseFloat(txtPrecio.getText());
                juego.setTitulo(titulo);
                juego.setPrecio(precio);
                //lo anado a la lista
                coleccionJuegos.add(juego);

                //Vacio los campos de texto despues de crear el juego
                limpiarCampos();
                listarJuegos();

                //Actualizo el contador de la label
                lblCantidadJuegos.setText("Juegos: " + coleccionJuegos.size());
            }
        });


        txtTitulo.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                super.keyReleased(e);

                //Obtener el String del campo de texto
                String titulo = txtTitulo.getText();
                //Contar su numero de caracteres
                int longitud = titulo.length();
                //Mostrar en la label la cantidad de caracteres
                lblLongitudTitulo.setText("Longitud: " + longitud);

            }
        });
        guardarDatosButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                guardarDatos();

            }
        });
        cargarDatosButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                leerDatos();
                //Ademas de cargarlos me interesa listarlos
                // para que aparezcan
                listarJuegos();
            }
        });
    }

    private void leerDatos() {
        try {
            //Leer el objeto guardado en el fichero (conjunto de juegos)
            FileInputStream fis = new FileInputStream("datos.bin");
            ObjectInputStream ois = new ObjectInputStream(fis);

            coleccionJuegos = (HashSet<Juego>) ois.readObject();

            fis.close();
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (ClassNotFoundException e1) {
            e1.printStackTrace();
        }
    }

    private void guardarDatos() {
        try {
             //Escribo el objeto
             FileOutputStream fos = new FileOutputStream("datos.bin");
             ObjectOutputStream oos = new ObjectOutputStream(fos);

             oos.writeObject(coleccionJuegos);

             fos.close();

         } catch (FileNotFoundException e1) {
             e1.printStackTrace();
         } catch (IOException e1) {
             e1.printStackTrace();
         }
    }

    private void limpiarCampos() {
        txtTitulo.setText("");
        txtPrecio.setText("");
    }

    public static void main(String[] args) {
        Ventana ventana = new Ventana();
    }


    public void listarJuegos(){
        //Recorre la coleccion de coleccionJuegos y lo va mostrando
        // en el area de texto
        //Para añadir texto o lineas al Area de texto usamos .append()

        //Elimino el texto del area de texto
        txtArea.setText("");

        //Anado una linea por cada juego
        for(Juego juego : coleccionJuegos){
            txtArea.append(juego + "\n");
        }

    }



}



