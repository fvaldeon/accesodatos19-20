package ficherotexto;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.Scanner;

/**
 * Created by Profesor on 27/09/2019.
 */
public class Ventana {


    private JTextField txtCampo;
    private JPanel panel1;
    private JRadioButton guardarRbtn;
    private JRadioButton cargarRbtn;
    private JButton archivoBtn;
    private JTextArea textArea;


    public Ventana() {
        archivoBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {


                if(guardarRbtn.isSelected()){
                    try {

                        guardarPalabraFichero(txtCampo.getText());

                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }else{

                    try {
                        cargarFichero();
                    } catch (FileNotFoundException e1) {
                        e1.printStackTrace();
                    }


                }

            }
        });
    }

    private void cargarFichero() throws FileNotFoundException {

        File fichero = new File("palabras.txt");
        Scanner input = new Scanner(fichero);

        while(input.hasNextLine()){
            textArea.append(input.nextLine() + "\n");
        }

        input.close();
    }

    private void guardarPalabraFichero(String text) throws IOException {

       FileWriter writer = new FileWriter("palabras.txt", true);
       PrintWriter pWriter = new PrintWriter(writer);
       pWriter.println(text);

       writer.close();

    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Ventana");
        frame.setContentPane(new Ventana().panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

        //Para mostrar en el centro de la pantalla
        frame.setLocationRelativeTo(null);
    }



}
