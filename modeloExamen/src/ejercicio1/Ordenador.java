package ejercicio1;

import java.time.LocalDate;

/**
 * Created by Profesor on 11/11/2019.
 */
public class Ordenador {
    private String modelo;
    private String marca;
    private float precio;
    private LocalDate fecha;

    public Ordenador(String modelo, String marca, float precio, LocalDate fecha) {
        this.modelo = modelo;
        this.marca = marca;
        this.precio = precio;
        this.fecha = fecha;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    @Override
    public String toString() {
        return modelo + " " + precio;
    }
}
