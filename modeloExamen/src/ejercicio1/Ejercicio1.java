package ejercicio1;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;

/**
 * Created by Profesor on 11/11/2019.
 */
public class Ejercicio1 {
    private JPanel panel1;
    private JTextField textField1;
    private JTextField textField2;
    private JTextField textField3;
    private JTextField textField4;
    private JButton altaButton;
    private JList list1;
    private DefaultListModel<Ordenador> dlm;


    public Ejercicio1() {
        dlm = new DefaultListModel<>();
        list1.setModel(dlm);

        altaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String modelo = textField1.getText();
                String marca = textField2.getText();
                float precio = Float.parseFloat(textField3.getText());
                LocalDate fecha = LocalDate.parse(textField4.getText());

                dlm.addElement(new Ordenador(modelo, marca, precio, fecha));
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Ejercicio1");
        frame.setContentPane(new Ejercicio1().panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
