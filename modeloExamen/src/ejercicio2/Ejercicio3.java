package ejercicio2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

/**
 * Created by Profesor on 11/11/2019.
 */
public class Ejercicio3 {
    private JTextField textField1;
    private JPanel panel1;
    private JButton listarPalabraButton;
    private JComboBox comboBox1;
    private JButton guardarFicheroButton;
    private DefaultComboBoxModel<String> dcbm;

    public Ejercicio3() {

        dcbm = new DefaultComboBoxModel<>();
        comboBox1.setModel(dcbm);

        listarPalabraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dcbm.addElement(textField1.getText());
            }
        });
        guardarFicheroButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                PrintWriter escritor = null;
                try {
                    escritor = new PrintWriter("fichero.txt");
                } catch (FileNotFoundException e1) {
                    e1.printStackTrace();
                }

                for(int i = 0; i < dcbm.getSize(); i++){
                    escritor.println(dcbm.getElementAt(i));
                }

                escritor.close();
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Ejercicio3");
        frame.setContentPane(new Ejercicio3().panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
