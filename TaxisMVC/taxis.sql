CREATE DATABASE compania_taxis;

use compania_taxis;

CREATE TABLE taxis(
id int PRIMARY KEY AUTO_INCREMENT,
marca VARCHAR(50),
modelo VARCHAR(50),
matricula VARCHAR(15) NOT NULL UNIQUE,
n_plazas int
);

CREATE TABLE conductores(
id int PRIMARY KEY AUTO_INCREMENT,
dni VARCHAR(15) UNIQUE,
nombre VARCHAR(50),
apellidos VARCHAR(100),
fecha_nacimiento DATE,
id_taxi int,
FOREIGN KEY (id_taxi) REFERENCES taxis(id)
);