package com.fvaldeon.taxis.mvc;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Profesor on 15/11/2019.
 */
public class Controlador implements ActionListener{

    private Vista vista;
    private Modelo modelo;

    public Controlador(Vista vista, Modelo modelo) {
        this.vista = vista;
        this.modelo = modelo;

        anadirActionListeners(this);
        modelo.conectar();

        listarTaxis();
    }

    private void anadirActionListeners(ActionListener listener){
        vista.eliminarBtn.addActionListener(listener);
        vista.insertarBtn.addActionListener(listener);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();

        switch (comando){
            case "Insertar":{
                String marca = vista.txtMarca.getText();
                String modeloTaxi = vista.txtModelo.getText();
                String matricula = vista.txtMatricula.getText();
                int numPlazas =  vista.slider1.getValue();
                modelo.insertarTaxi(marca, modeloTaxi, matricula, numPlazas);

                listarTaxis();
            }
            break;

            case "Eliminar":{
                //modelo.eliminarTaxi();
            }
            break;

            case "itemSalir":{

                System.exit(0);
            }
            break;
        }

    }

    private void listarTaxis(){
        ResultSet resultado = modelo.obtenerTaxisResultSet();

        //Elimino los registros anteriores de la tabla
        vista.dtmTaxis.setRowCount(0);

        try {
            while(resultado.next()){
                //Obtengo los valores de las columnas
                //del conjunto de registros (ResultSet)
                int id = resultado.getInt(1);
                String marca = resultado.getString(2);
                String modelo = resultado.getString(3);
                String matricula = resultado.getString(4);
                int numPlazas = resultado.getInt(5);

                //Añado al dtm un array con los valores de una fila
                vista.dtmTaxis.addRow(new Object[]{id, marca, modelo, matricula, numPlazas});
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
