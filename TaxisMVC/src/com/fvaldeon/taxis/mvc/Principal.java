package com.fvaldeon.taxis.mvc;

/**
 * Created by Profesor on 15/11/2019.
 */
public class Principal {

    public static void main(String[] args) {
        Vista vista = new Vista();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(vista, modelo);
    }
}
