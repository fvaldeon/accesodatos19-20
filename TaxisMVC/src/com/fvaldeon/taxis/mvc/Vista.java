package com.fvaldeon.taxis.mvc;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

/**
 * Created by Profesor on 15/11/2019.
 */
public class Vista {
    private JFrame frame;
    private JTabbedPane tabbedPane1;
    private JPanel panel1;
    JTextField txtMarca;
    JTextField txtModelo;
    JTextField txtMatricula;
    JSlider slider1;
    JTable tableTaxis;
    JButton eliminarBtn;
    JButton insertarBtn;
    DefaultTableModel dtmTaxis;
    JMenuItem itemSalir;

    public Vista() {
        frame = new JFrame("Vista");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        crearMenu();

        frame.pack();
        frame.setVisible(true);

        frame.setLocationRelativeTo(null);

        inicializarTabla();

    }

    private void inicializarTabla(){
        dtmTaxis = new DefaultTableModel();
        tableTaxis.setModel(dtmTaxis);

        Object[] cabeceras = {"id","marca","modelo","matricula","n_plazas"};
        dtmTaxis.setColumnIdentifiers(cabeceras);


    }

    private void crearMenu(){
        JMenuBar barra = new JMenuBar();
        JMenu menu = new JMenu("Archivo");
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("itemSalir");

        menu.add(itemSalir);
        barra.add(menu);
        frame.setJMenuBar(barra);
    }

}
