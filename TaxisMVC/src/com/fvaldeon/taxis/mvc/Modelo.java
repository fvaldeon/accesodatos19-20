package com.fvaldeon.taxis.mvc;

import java.sql.*;

/**
 * Created by Profesor on 15/11/2019.
 */
public class Modelo {

    private Connection conexion;

    public void conectar(){
        try {
            conexion = DriverManager.getConnection("jdbc:mysql://localhost:3307/compania_taxis","root", "");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void desconectar(){
        try {
            conexion.close();
            conexion = null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insertarTaxi(String marca, String modelo, String matricula, int numPlazas){
        String consulta = "INSERT INTO taxis(marca, modelo, n_plazas, matricula) VALUES(?, ?, ?, ?)" ;

        PreparedStatement stmt = null;
        try {
            //Creo una consulta precompilada en el servidor de bbdd
            stmt = conexion.prepareStatement(consulta);
            stmt.setString(1, marca);
            stmt.setString(2, modelo);
            stmt.setInt(3, numPlazas);
            stmt.setString(4, matricula);

            //Ejecuto la consulta precompilada
            stmt.executeUpdate();

            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void eliminarTaxi(int id){
        String consulta = "DELETE FROM taxis WHERE id = ?";

        try {
            PreparedStatement sentencia = conexion.prepareStatement(consulta);

            sentencia.setInt(1, id);
            sentencia.executeUpdate();

            sentencia.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void modificarTaxi(int id, String marca, String modelo, String matricula, int numPlazas){
        String consulta = "UPDATE taxis SET marca = ?, modelo = ?, matricula = ?" +
                ", n_plazas = ? WHERE id = ?";

        try {
            PreparedStatement sentencia = conexion.prepareStatement(consulta);

            sentencia.setString(1, marca);
            sentencia.setString(2, modelo);
            sentencia.setString(3, matricula);
            sentencia.setInt(4, numPlazas);
            sentencia.setInt(5, id);

            sentencia.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public ResultSet obtenerTaxisResultSet(){
        String consulta = "SELECT * FROM taxis";
        ResultSet resultado = null;

        try {
            PreparedStatement statement = conexion.prepareStatement(consulta);

            resultado = statement.executeQuery();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return resultado;

    }

}
