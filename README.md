** Ejercicios realizados de Acceso a Datos 19-20 **

   * **PrimerEjemplo**: Aplicacion inicial con ventana, para refrescar contenidos y a modo introduccion. ActionListener, KeyListener, campos de texto y area de texto. Trabajamos con fichero de texto y fichero binarios.
   * **VentanaConfiguracion**: Proyecto con 2 ventanas que guardan su estado utilizando ficheros de configuracion. Checkbox, RadioButton, Spinner, JFileChooser, etc.
   * **BlocNotas**: Primer proyecto real, usa ficheros de texto y fichero de configuracion. Bloc de notas que me permite guardar fichero en cualquier lugar, y al abrirlo me carga automaticamente el último editado. JMenuBar, JFileChooser.
   * **PasteleriaMVC**: Ejemplo de patron Modelo Vista Controlador. ActionListener, KeyListener, ListSelectionListener y WindowListener. JDialog.
   * **ComboBoxCoches**: Uso de JComboBox y DefaultComboBoxModel. Eliminar desde teclado, exportar/importar XML
   * **Modelo Examen**: Solucion a algunos ejercicios del modelo de examen colgado en edmodo
   * **TaxisMVC**: Aplicación MVC que conecta con Servidor mysql bbdd