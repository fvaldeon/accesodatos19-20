package com.fvaldeon.notepad;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.Properties;
import java.util.Scanner;

/**
 * Created by Profesor on 07/10/2019.
 */
public class VentanaPrincipal {
    private JFrame frame;
    private JPanel panel1;
    private JTextArea txtArea;


    public VentanaPrincipal() {
        frame = new JFrame("NotePad");

        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        iniciarMenu();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);

        try {
            cargarFicheroAnterior();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void cargarFicheroAnterior() throws IOException {
        File fichero = obtenerFicheroAnterior();

        Scanner lector = new Scanner(fichero);
        txtArea.setText("");
        while(lector.hasNextLine()){
            txtArea.append(lector.nextLine() + "\n");
        }
        lector.close();
    }

    private File obtenerFicheroAnterior() throws IOException {
        Properties conf = new Properties();
        conf.load(new FileReader("notepad.conf"));
        String ruta = conf.getProperty("ruta_ultimo_fichero");
        return new File(ruta);
    }

    private void iniciarMenu() {
        JMenuBar barra = new JMenuBar();
        JMenu menuArchivo = new JMenu("Archivo");
        JMenuItem itemGuardar = new JMenuItem("Guardar");

        menuArchivo.add(itemGuardar);
        barra.add(menuArchivo);
        frame.setJMenuBar(barra);


        itemGuardar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //Con JFileChooser puedo seleccionar donde guardar
                JFileChooser selector = new JFileChooser();
                int opcion = selector.showSaveDialog(null);

                //Si he pulsado el boton aceptar
                if(opcion == JFileChooser.APPROVE_OPTION){
                    //Guardo el texto en el fichero
                    try {

                        File fichero = selector.getSelectedFile();
                        PrintWriter escritor = new PrintWriter(fichero);
                        escritor.println(txtArea.getText());
                        escritor.close();

                        //Creo un fichero de configuracion
                        //En el que almaceno la ruta del
                        // archivo que he guardado
                        actualizarFicheroConfiguracion(fichero.getAbsolutePath());

                    } catch (FileNotFoundException e1) {
                        e1.printStackTrace();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });
    }

    private void actualizarFicheroConfiguracion(String absolutePath) throws IOException {
        Properties config = new Properties();
        config.setProperty("ruta_ultimo_fichero", absolutePath);
        config.store(new FileWriter("notepad.conf"), "Configuracion de notepad");
    }

}
