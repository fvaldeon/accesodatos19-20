package com.fvaldeon.pasteleriamvc;

import com.fvaldeon.pasteleriamvc.gui.Controlador;
import com.fvaldeon.pasteleriamvc.gui.Modelo;
import com.fvaldeon.pasteleriamvc.gui.Vista;
import com.fvaldeon.pasteleriamvc.util.Util;

import java.io.File;
import java.io.IOException;

/**
 * Created by DAM on 14/10/2019.
 */
public class Principal {

    public static void main(String[] args) {
        Vista vista = new Vista();
        Modelo modelo = new Modelo();

        crearDirectorios();

        try {
            modelo.cargarDatosDeFichero();
        } catch (IOException e) {
            e.printStackTrace();
            Util.mostrarMensajeError("Error al cargar los datos\n" + e.getLocalizedMessage());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            Util.mostrarMensajeError("Los datos que se intentan cargar son inconsistentes");
        }

        Controlador controlador = new Controlador(vista, modelo);

    }

    private static void crearDirectorios(){
        File directorio = new File("datos");
        if(!directorio.exists()) {
           directorio.mkdir();
       }
    }
}
