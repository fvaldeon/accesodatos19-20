package com.fvaldeon.pasteleriamvc.gui;

import com.fvaldeon.pasteleriamvc.base.Bizcocho;
import com.fvaldeon.pasteleriamvc.base.Pasteleria;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;

/**
 * Created by DAM on 14/10/2019.
 */
public class Vista {
    JFrame frame;
    private JPanel panel1;
    JTextField txtNombrePasteleria;
    JTextField txtDireccionPasteleria;
    JList<Bizcocho> listBizcochosDePasteleria;
    JButton eliminarPasteleriaBtn;
    JButton nuevaPasteleriaBtn;
    JTextField txtNombreBizcocho;
    JTextField txtValorEnergetico;
    JTextArea txtAreaIngredientes;
    JList<Bizcocho> listBizcochos;
    JButton eliminarBizcochoBtn;
    JButton nuevoBizcochoBtn;
    DatePicker selectorFecha;
    JList<Pasteleria> listPastelerias;
    JButton seleccionarBizcochosBtn;
    JComboBox<String> comboBoxBuscarBizcocho;
    JTextField txtBuscarBizcocho;

    DefaultListModel<Pasteleria> dlmPasteleria;
    DefaultListModel<Bizcocho> dlmBizcocho;
    DefaultListModel<Bizcocho> dlmBizcochoDePasteleria;

    public Vista() {
        frame = new JFrame("Vista");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);

        inicializarModelos();

    }

    private void inicializarModelos() {
        dlmBizcochoDePasteleria = new DefaultListModel<>();
        dlmBizcocho = new DefaultListModel<>();
        dlmPasteleria = new DefaultListModel<>();
        listBizcochos.setModel(dlmBizcocho);
        listBizcochosDePasteleria.setModel(dlmBizcochoDePasteleria);
        listPastelerias.setModel(dlmPasteleria);
    }

}
