package com.fvaldeon.pasteleriamvc.gui;

import com.fvaldeon.pasteleriamvc.base.Bizcocho;
import com.fvaldeon.pasteleriamvc.base.Pasteleria;
import com.fvaldeon.pasteleriamvc.util.Util;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.*;
import java.io.IOException;
import java.time.LocalDate;

/**
 * Created by DAM on 14/10/2019.
 */
public class Controlador implements ActionListener, KeyListener, ListSelectionListener, WindowListener{

    private Vista vista;
    private Modelo modelo;

    public Controlador(Vista vista, Modelo modelo) {

        this.vista = vista;
        this.modelo = modelo;

        addListeners();

        listarBizcochos();
        listarPastelerias();
    }

    private void addListeners() {
        //Anado los listeners para los eventos sobre los botones
        addActionListeners(this);

        //Anado los listeners para lo eventos de teclado
        addKeyListeners(this);

        addSelectionListeners(this);

        addWindowListeners(this);
    }


    private void addActionListeners(ActionListener listener){


        vista.eliminarBizcochoBtn.addActionListener(listener);
        vista.eliminarPasteleriaBtn.addActionListener(listener);
        vista.nuevaPasteleriaBtn.addActionListener(listener);
        vista.nuevoBizcochoBtn.addActionListener(listener);
        vista.seleccionarBizcochosBtn.addActionListener(listener);

    }

    private void addKeyListeners(KeyListener listener){
        vista.listPastelerias.addKeyListener(listener);
        vista.listBizcochos.addKeyListener(listener);
        vista.txtBuscarBizcocho.addKeyListener(listener);
    }

    private void addSelectionListeners(ListSelectionListener listener){
        vista.listPastelerias.addListSelectionListener(listener);
        vista.listBizcochos.addListSelectionListener(listener);
    }

    private void addWindowListeners(WindowListener listener){
        vista.frame.addWindowListener(listener);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();

        switch (comando) {



            case "NuevaPasteleria": {

                String nombre = vista.txtNombrePasteleria.getText();
                String direccion = vista.txtDireccionPasteleria.getText();
                LocalDate fecha = vista.selectorFecha.getDate();
                modelo.nuevaPasteleria(nombre, direccion, fecha);
                limpiarCamposPasteleria();
                listarPastelerias();

            }
            break;

            case "NuevoBizcocho": {
                modelo.nuevoBizcocho(vista.txtNombreBizcocho.getText(), vista.txtAreaIngredientes.getText(), Double.parseDouble(vista.txtValorEnergetico.getText()));
                limpiarCamposBizcochos();
                listarBizcochos();
            }
            break;

            case "EliminarPasteleria": {
                Pasteleria pasteleria = vista.listPastelerias.getSelectedValue();
                modelo.eliminarPasteleria(pasteleria);

                listarPastelerias();
            }
                break;



            case "EliminarBizcocho": {
                modelo.eliminarBizcocho(vista.listBizcochos.getSelectedValue());

                listarBizcochos();
            }
                break;

            case "SeleccionarBizcochos":{
                if(vista.listPastelerias.isSelectionEmpty()){
                    Util.mostrarMensajeError("Debes seleccionar una pasteleria");
                }else {
                    Pasteleria pasteleria = vista.listPastelerias.getSelectedValue();

                    AdministrarBizcochosDialog dialog = new AdministrarBizcochosDialog(pasteleria, modelo.getBizcochos());
                    dialog.mostrarDialogo();

                    //refresco los bizcochos de esta pateleria
                    listarBizcochosDePasteleria(pasteleria);
                }
            }
            break;

        }

    }
        private void listarBizcochosDePasteleria(Pasteleria seleccionada){
            vista.dlmBizcochoDePasteleria.clear();
            for(Bizcocho bizcocho : seleccionada.getStockBiscochos()){
                vista.dlmBizcochoDePasteleria.addElement(bizcocho);
            }
        }

        private void listarPastelerias(){
            vista.dlmPasteleria.clear();
            for(Pasteleria pasteleria : modelo.getPastelerias()){
                vista.dlmPasteleria.addElement(pasteleria);
            }
        }


        private void listarBizcochos(){
            vista.dlmBizcocho.clear();
            for(Bizcocho bizcocho : modelo.getBizcochos()){
                vista.dlmBizcocho.addElement(bizcocho);
            }
        }

        //Metodo para buscar por un valor
        private void listarBizcochos(String valorBusqueda, String campo){
            vista.dlmBizcocho.clear();
            for(Bizcocho bizcocho : modelo.getBizcochos()){
                if(campo.equals("Nombre")) {
                    if(bizcocho.getNombre().contains(valorBusqueda)) {
                        vista.dlmBizcocho.addElement(bizcocho);
                    }
                } else if (campo.equals("Ingredientes")){
                    if(bizcocho.getIngredientes().contains(valorBusqueda)){
                        vista.dlmBizcocho.addElement(bizcocho);
                    }
                } else {

                    if(bizcocho.getValorEnergetico() == Double.parseDouble(valorBusqueda)){
                        vista.dlmBizcocho.addElement(bizcocho);
                    }
                }
            }
        }

        private void limpiarCamposPasteleria(){
            vista.txtNombrePasteleria.setText("");
            vista.txtDireccionPasteleria.setText("");
            vista.selectorFecha.setText("");
        }

        private void limpiarCamposBizcochos(){
            vista.txtNombreBizcocho.setText("");
            vista.txtAreaIngredientes.setText("");
            vista.txtValorEnergetico.setText("");
        }

        private void mostrarCamposPasteleria(Pasteleria pasteleria){
            vista.txtNombrePasteleria.setText(pasteleria.getNombre());
            vista.txtDireccionPasteleria.setText(pasteleria.getDireccion());
            vista.selectorFecha.setDate(pasteleria.getFechaApertura());
        }

        private void mostrarCamposBizcochos(Bizcocho bizcocho){
            vista.txtNombreBizcocho.setText(bizcocho.getNombre());
            vista.txtAreaIngredientes.setText(bizcocho.getIngredientes());
            vista.txtValorEnergetico.setText(String.valueOf(bizcocho.getValorEnergetico()));
        }



    @Override
    public void keyReleased(KeyEvent e) {
        if(e.getSource() == vista.txtBuscarBizcocho){
            if (vista.comboBoxBuscarBizcocho.getSelectedItem().equals("Nombre") ||
                vista.comboBoxBuscarBizcocho.getSelectedItem().equals("Ingredientes")) {

                //Busco por nombre e ingredientes solo si hay 3 caracteres minimo
                if (vista.txtBuscarBizcocho.getText().length() >= 3) {
                    String valorBusqueda = vista.txtBuscarBizcocho.getText();
                    String campo = (String) vista.comboBoxBuscarBizcocho.getSelectedItem();

                    listarBizcochos(valorBusqueda, campo);
                } else {
                    listarBizcochos();
                }
            } else {
                if(vista.txtBuscarBizcocho.getText().isEmpty()) {
                    listarBizcochos();
                } else {
                    String valorBusqueda = vista.txtBuscarBizcocho.getText();
                    String campo = (String) vista.comboBoxBuscarBizcocho.getSelectedItem();
                    listarBizcochos(valorBusqueda, campo);
                }
            }
        } else {
            if (e.getKeyCode() == KeyEvent.VK_DELETE) {
                if (e.getSource() == vista.listPastelerias) {

                    Pasteleria pasteleria = vista.listPastelerias.getSelectedValue();
                    modelo.eliminarPasteleria(pasteleria);
                    limpiarCamposPasteleria();
                    listarPastelerias();

                } else if (e.getSource() == vista.listBizcochos) {

                    Bizcocho bizcocho = vista.listBizcochos.getSelectedValue();
                    modelo.eliminarBizcocho(bizcocho);
                    limpiarCamposBizcochos();
                    listarBizcochos();
                }
            }
        }
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
            if(e.getSource() == vista.listPastelerias && !vista.listPastelerias.isSelectionEmpty()) {
                Pasteleria pasteleria = vista.listPastelerias.getSelectedValue();
                listarBizcochosDePasteleria(pasteleria);
                //Muestro los valores del pastel seleccionado
                mostrarCamposPasteleria(pasteleria);

            }else if(e.getSource() == vista.listBizcochos && !vista.listBizcochos.isSelectionEmpty()){

                Bizcocho bizcocho = vista.listBizcochos.getSelectedValue();
                mostrarCamposBizcochos(bizcocho);
            }
    }

    @Override
    public void windowClosing(WindowEvent e) {
        //Cuando se esta cerrando
        try {
            modelo.guardarDatosEnFichero();
        } catch (IOException e1) {
            e1.printStackTrace();
            Util.mostrarMensajeError("Error al guardar datos");
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }


    @Override
    public void windowOpened(WindowEvent e) {

    }



    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}

