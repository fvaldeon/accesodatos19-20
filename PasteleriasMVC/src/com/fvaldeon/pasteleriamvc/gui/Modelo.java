package com.fvaldeon.pasteleriamvc.gui;

import com.fvaldeon.pasteleriamvc.base.Bizcocho;
import com.fvaldeon.pasteleriamvc.base.Pasteleria;

import java.io.*;
import java.time.LocalDate;
import java.util.HashSet;

/**
 * Created by DAM on 14/10/2019.
 */
public class Modelo {
    private HashSet<Pasteleria> pastelerias;
    private HashSet<Bizcocho> bizcochos;

    public Modelo() {
        pastelerias = new HashSet<>();
        bizcochos = new HashSet<>();
    }

    public HashSet<Pasteleria> getPastelerias() {
        return pastelerias;
    }

    public HashSet<Bizcocho> getBizcochos() {
        return bizcochos;
    }

    public void nuevaPasteleria(String nombre, String direccion, LocalDate fechaApertura){
        pastelerias.add(new Pasteleria(nombre, direccion, fechaApertura));
    }

    public void nuevoBizcocho(String nombre, String ingredientes, double valorEnergetico){
        bizcochos.add(new Bizcocho(nombre, ingredientes, valorEnergetico));
    }

    public void eliminarPasteleria(Pasteleria pasteleria){
        pastelerias.remove(pasteleria);
    }

    public void eliminarBizcocho(Bizcocho bizcocho){
        bizcochos.remove(bizcocho);
    }

    public void guardarDatosEnFichero() throws IOException {
        FileOutputStream flujoFichero = new FileOutputStream("datos/datos.bin");
        ObjectOutputStream serializador = new ObjectOutputStream(flujoFichero);

        serializador.writeObject(pastelerias);
        serializador.writeObject(bizcochos);

        flujoFichero.close();
    }

    public void cargarDatosDeFichero() throws IOException, ClassNotFoundException {
        FileInputStream flujoFichero = new FileInputStream("datos/datos.bin");
        ObjectInputStream deserializador = new ObjectInputStream(flujoFichero);

        pastelerias = (HashSet<Pasteleria>) deserializador.readObject();
        bizcochos = (HashSet<Bizcocho>) deserializador.readObject();

        flujoFichero.close();
    }

}
