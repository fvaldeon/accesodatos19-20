package com.fvaldeon.pasteleriamvc.gui;

import com.fvaldeon.pasteleriamvc.base.Bizcocho;
import com.fvaldeon.pasteleriamvc.base.Pasteleria;

import javax.swing.*;
import java.awt.event.*;
import java.util.HashSet;

public class AdministrarBizcochosDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JList<Bizcocho> listBizcochosDisponibles;
    private JList<Bizcocho> listBizcochosActuales;
    private JButton quitarBizcochoBtn;
    private JButton anadirBizcochoBtn;
    private DefaultListModel<Bizcocho> dlmBizcochosDisponibles;
    private DefaultListModel<Bizcocho> dlmBizcochosActuales;
    private Pasteleria pasteleria;
    private HashSet<Bizcocho> bizcochosDisponibles;


    public AdministrarBizcochosDialog(Pasteleria pasteleria, HashSet<Bizcocho> bizcochos) {

        this.pasteleria = pasteleria;
        this.bizcochosDisponibles = bizcochos;

        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        setLocationRelativeTo(null);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        quitarBizcochoBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //Al pulsar sobre quitar, se eliminara el bizcocho
                //Seleccionado de la pasteleria
                Bizcocho seleccionado = listBizcochosActuales.getSelectedValue();
                pasteleria.getStockBiscochos().remove(seleccionado);
                listarBizcochosActuales();

            }
        });
        anadirBizcochoBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                /*
                Al pulsar añadir, se añadira el bizcocho
                seleccionado a la pasteleria
                 */

                Bizcocho seleccionado = listBizcochosDisponibles.getSelectedValue();
                pasteleria.getStockBiscochos().add(seleccionado);
                listarBizcochosActuales();

            }
        });

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        pack();

        iniciarListas();
        setTitle("Pateleria: " + pasteleria);

    }

    private void onOK() {
        dispose();
    }

    public void mostrarDialogo(){
        listarDatos();
        setVisible(true);
    }

    private void iniciarListas(){
        dlmBizcochosDisponibles = new DefaultListModel<>();
        dlmBizcochosActuales = new DefaultListModel<>();
        listBizcochosActuales.setModel(dlmBizcochosActuales);
        listBizcochosDisponibles.setModel(dlmBizcochosDisponibles);
    }

    private void listarDatos(){
        //Listo los bizcochos de la pasteleria actual
        listarBizcochosActuales();

        //Listo los bizcochos disponibles
        for(Bizcocho bizcocho : bizcochosDisponibles){
            dlmBizcochosDisponibles.addElement(bizcocho);
        }
    }

    private void listarBizcochosActuales() {
        dlmBizcochosActuales.clear();
        for(Bizcocho bizcocho : pasteleria.getStockBiscochos()){
            dlmBizcochosActuales.addElement(bizcocho);
        }
    }


    //Metodo listar bizcochos y listar bizcochos disponibles

}
