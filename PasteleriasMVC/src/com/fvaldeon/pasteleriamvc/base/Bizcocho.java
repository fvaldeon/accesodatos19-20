package com.fvaldeon.pasteleriamvc.base;

import java.io.Serializable;

/**
 * Created by DAM on 14/10/2019.
 */
public class Bizcocho implements Serializable{

    private String nombre;
    private String ingredientes;
    private double valorEnergetico;

    public Bizcocho(String nombre, String ingredientes, double valorenergetico) {
        this.nombre = nombre;
        this.ingredientes = ingredientes;
        this.valorEnergetico = valorenergetico;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIngredientes() {
        return ingredientes;
    }

    public void setIngredientes(String ingredientes) {
        this.ingredientes = ingredientes;
    }

    public double getValorEnergetico() {
        return valorEnergetico;
    }

    public void setValorEnergetico(double valorEnergetico) {
        this.valorEnergetico = valorEnergetico;
    }

    @Override
    public String toString() {
        return nombre + ": " + ingredientes;
    }
}
