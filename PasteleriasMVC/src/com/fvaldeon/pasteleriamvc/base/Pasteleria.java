package com.fvaldeon.pasteleriamvc.base;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;

/**
 * Created by DAM on 14/10/2019.
 */
public class Pasteleria implements Serializable {

    private String nombre;
    private String direccion;
    private LocalDate fechaApertura;
    private HashSet<Bizcocho> stockBiscochos;

    public Pasteleria(String nombre, String direccion, LocalDate fechaApertura) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.fechaApertura = fechaApertura;
        this.stockBiscochos = new HashSet<Bizcocho>();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public LocalDate getFechaApertura() {
        return fechaApertura;
    }

    public void setFechaApertura(LocalDate fechaApertura) {
        this.fechaApertura = fechaApertura;
    }

    public HashSet<Bizcocho> getStockBiscochos() {
        return stockBiscochos;
    }

    public void setStockBiscochos(HashSet<Bizcocho> stockBiscochos) {
        this.stockBiscochos = stockBiscochos;
    }

    @Override
    public String toString() {
        return nombre + " : " + direccion;
    }
}
