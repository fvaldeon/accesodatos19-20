package aplicacionConfiguracionSencilla;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by Profesor on 04/10/2019.
 */
public class ConfiguracionSencilla {

    private JPanel panel1;
    private JTextField txtIP;
    private JTextField txtPuerto;
    private JPasswordField txtPassword;
    private JButton guardarBtn;
    private JButton cargarBtn;


    public ConfiguracionSencilla() {
        guardarBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                guardarPropiedades();

            }
        });
        cargarBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                cargarPropiedades();

            }
        });
    }

    private void cargarPropiedades() {
        //Utilizo JFileChooser para seleccionar la ruta
        //desde la que quiero cargar el fichero
        JFileChooser selector = new JFileChooser();

        int opcion = selector.showOpenDialog(null);

        if(opcion == JFileChooser.APPROVE_OPTION) {

            Properties conf = new Properties();

            try {
                File fichero = selector.getSelectedFile();
                conf.load(new FileReader(fichero));

                txtIP.setText(conf.getProperty("ip"));
                txtPuerto.setText(conf.getProperty("puerto"));
                txtPassword.setText(conf.getProperty("password"));

            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }

    private void guardarPropiedades() {

        JFileChooser selector = new JFileChooser();
        //Si la opcion que me devuelve el metodo showSaveDialog
        //es igual a 0 (APPROVE_OPTION -> 0), entonces creo un
        //objeto properties y lo guardo en la ruta del fichero
        //seleccionado
        int opcion = selector.showSaveDialog(null);

        if(opcion == JFileChooser.APPROVE_OPTION) {
            Properties conf = new Properties();
            conf.setProperty("ip", txtIP.getText());
            conf.setProperty("puerto", txtPuerto.getText());
            conf.setProperty("password", String.valueOf(txtPassword.getPassword()));

            try {
                File fichero = selector.getSelectedFile();
                conf.store(new FileWriter(fichero), "Fichero de configuracion 2");
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("ConfiguracionSencilla");
        frame.setContentPane(new ConfiguracionSencilla().panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

        frame.setLocationRelativeTo(null);
    }
}
