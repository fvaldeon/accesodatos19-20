package aplicacionConfiguracion1;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.Properties;

/**
 * Created by Profesor on 30/09/2019.
 */
public class VentanaConfiguracion {
    private JPanel panel1;
    private JTextField txtHost;
    private JCheckBox anonymousCB;
    private JRadioButton mysqlRB;
    private JSpinner clientesSpinner;
    private JButton guardarBtn;
    private JRadioButton postgreRB;


    public VentanaConfiguracion() {

        guardarBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                Properties configuracion = new Properties();
                configuracion.setProperty("host", txtHost.getText());
                configuracion.setProperty("anonymous", String.valueOf(anonymousCB.isSelected()));
                configuracion.setProperty("mysql", String.valueOf(mysqlRB.isSelected()));
                configuracion.setProperty("n_clientes", String.valueOf(clientesSpinner.getValue()));

                try {
                    configuracion.store(new FileWriter("propiedades.conf"), "Configuracion  de mi ventana");
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });

        try {
            cargarPropiedades();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void cargarPropiedades() throws IOException {

        Properties configuracion = new Properties();
        configuracion.load(new FileReader("propiedades.conf"));

        String host = configuracion.getProperty("host");
        txtHost.setText(host);

        String anonymous = configuracion.getProperty("anonymous");
        anonymousCB.setSelected(Boolean.parseBoolean(anonymous));

        String mysql = configuracion.getProperty("mysql");
        mysqlRB.setSelected(Boolean.parseBoolean(mysql));
        postgreRB.setSelected(!Boolean.parseBoolean(mysql));

        String nClientes = configuracion.getProperty("n_clientes");
        clientesSpinner.setValue(Integer.parseInt(nClientes));

    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("VentanaConfiguracion");
        frame.setContentPane(new VentanaConfiguracion().panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

        frame.setLocationRelativeTo(null);

    }
}
